from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)


class hotel_restaurant_tables(models.Model):

    _inherit = "hotel.restaurant.tables"

    order_id = fields.Many2one(
        'hotel.restaurant.order',
        string='order active',
        compute='compute_order_id'
    )
    sizex = fields.Float(
        string='x',
    )

    sizey = fields.Float(
        string='y',
    )

    posx = fields.Float(
        string='x pos',
    )
    posy = fields.Float(
        string='y pos',
    )
    shape = fields.Char(
        string='shape',
        default="square"
    )
    color = fields.Char(
        string='color',
        default="blue"
    )

    @api.one
    def compute_order_id(self):
        order_id = self.env['hotel.restaurant.order'].search([
            ('state', 'in', ['draft', 'order']),
            ('table_no', '=', self.id)], limit=1)
        if len(order_id):
            self.order_id = order_id.id

    @api.multi
    @api.returns('hotel.restaurant.order')
    def get_order(self):
        order_id = self.env['hotel.restaurant.order'].search([
            ('state', 'in', ['draft', 'order']),
            ('table_no', 'in', self.ids)], limit=1)
        if len(order_id):
            return order_id
        else:
            return self.env['hotel.restaurant.order'].create({
                'cname': self.env.ref('hotel_restaurant_stock.defaul_partner').id,
                'table_no': [(6, 0, self.ids)],
                'waiter_name': self.env.user.partner_id.id
            },
            )


class hotel_restaurant_order(models.Model):
    _inherit = 'hotel.restaurant.order'

    stock_picking_id = fields.Many2one(
        'stock.picking',
        string='stock picking',
    )
    discount = fields.Float(
        string='discount',
        default=0
    )
    invoice_id = fields.Many2one(
        'account.invoice',
        string='invoice',
    )
    currency_id = fields.Many2one(
        'res.currency',
        related='invoice_id.currency_id',
        related_sudo=False
    )

    residual = fields.Monetary(
        string='Amount Due',
        related='invoice_id.residual',
        help="Remaining amount due."
    )

    @api.one
    def generate_update_kot(self):
        if self.state == 'draft':
            kot = self.new_generate_kot()
            try:
                print_product_ids = kot.kot_list.mapped('name').filtered(lambda x: x.print_kot is True)
                if len(print_product_ids):
                    self.env['report'].print_document(
                        [kot.id], 'hotel_restaurant_stock.tiket_kot')
                kot.action_done()
                return True
            except:
                return False
        elif self.state == 'order':
            try:
                kot = self.new_generate_kot()
                print_product_ids = kot.kot_list.mapped('name').filtered(lambda x: x.print_kot is True)
                if len(print_product_ids):

                    self.env['report'].print_document(
                        [kot.id], 'hotel_restaurant_stock.tiket_kot')
                kot.action_done()
                return True
            except:
                return False

    @api.one
    def pay_residual(self, print_me, journal_id=False, amount=0):
        if journal_id:
            ctx = {'active_model': 'account.invoice',
                   'active_ids': [self.invoice_id.id]}

            register_payments = self.env['account.register.payments'].with_context(ctx).create({
                'payment_date': fields.Date.today(),
                'journal_id': journal_id,
                'payment_method_id': self.env.ref("account.account_payment_method_manual_in").id,
            })
            if amount:
                register_payments.amount = amount

            register_payments.create_payment()

        if print_me:
            try:
                self.env['report'].print_document(
                    [self.invoice_id.id], 'hotel_restaurant_stock.account_invoices_comandera')
            except :
                _logger.info(
                    'No se pudo imprimir hotel_restaurant_stock.account_invoices_comandera')
        return True

    @api.one
    def done_invoice(self, print_me, journal_id=False, amount=0, discount=0.0):
        self.done_order_kot()

        inv = {}
        inv['partner_id'] = self.cname.id
        inv['account_id'] = self.cname.property_account_receivable_id.id
        inv['invoice_line_ids'] = []
        inv['journal_id'] = self.env.ref(
            'hotel_restaurant_stock.rest_jorunal').id
        inv['comment'] = 'Servicio a ' + \
            " ".join([x.name for x in self.table_no])

        for item in self.order_list:
            if item.state == 'cancel':
                continue
            account_id = item.name.product_id.property_account_income_id or item.name.product_id.categ_id.property_account_income_categ_id
            inv['invoice_line_ids'].append((0, 0, {
                'product_id': item.name.product_id.id,
                'name': item.name.name,
                'account_id': account_id.id,
                'quantity': item.item_qty,
                'price_unit': item.item_rate
            }))

        if discount:
            self.discount = discount
            discount_amount = discount * -1
            product_product_discount = self.env.ref(
                'hotel_restaurant_stock.product_product_discount')
            account_id = item.name.product_id.property_account_income_id or item.name.product_id.categ_id.property_account_income_categ_id

            inv['invoice_line_ids'].append((0, 0, {
                'product_id': product_product_discount.id,
                'name': 'Descuento ',
                'account_id': account_id.id,
                'quantity': 1,
                'price_unit': discount_amount
            }))

        invoice_id = self.env['account.invoice'].create(inv)
        self.invoice_id = invoice_id

        if journal_id:
            invoice_id.action_invoice_open()
            ctx = {'active_model': 'account.invoice',
                   'active_ids': [invoice_id.id]}

            register_payments = self.env['account.register.payments'].with_context(ctx).create({
                'payment_date': fields.Date.today(),
                'journal_id': journal_id,
                'payment_method_id': self.env.ref("account.account_payment_method_manual_in").id,
            })
            if amount:
                register_payments.amount = amount

            register_payments.create_payment()

        if print_document:
            self.env.cr.commit()
            #_logger.info('%r' % invoice_id)
            try:
                self.env['report'].print_document(
                        [invoice_id.id], 'hotel_restaurant_stock.report_invoice_comandera')
            except:
                _logger.info(
                    'No se pudo imprimir account.report_invoice_document')
        return True


# Comanda de pedido
class hotel_restaurant_kitchen_order_tickets(models.Model):
    _inherit = 'hotel.restaurant.kitchen.order.tickets'

    state = fields.Selection(
        [('recept', 'recept'), ('done', 'done')],
        string='State',

        default='recept'
    )

    stock_picking_id = fields.Many2one(
        'stock.picking',
        string='stock picking',
    )

    table_names = fields.Char(
        string='Tables',
        compute="_compute_table_names"
    )

    @api.one
    def action_done(self):
        self.kot_list.action_done()

        self.state = 'done'

    @api.one
    @api.depends('tableno')
    def _compute_table_names(self):
        table_names = ""
        for tableno in self.tableno:
            table_names += tableno.name

        self.table_names = table_names


class hotel_restaurant_order_list(models.Model):
    _inherit = 'hotel.restaurant.order.list'
    _order = 'item_order asc'

    comments = fields.Char(
        string='comments',
    )
    state = fields.Selection(
        [('draft', 'draft'), ('preparation', 'preparation'),
         ('ok', 'ok'), ('done', 'done'), ('cancel', 'cancel')],
        string='state',
        default="draft"
    )
    move_id = fields.Many2one(
        'stock.move',
        string='move',
    )
    item_order = fields.Integer(
        string='orden',
    )

    @api.model
    def create(self, vals):
        res = super(hotel_restaurant_order_list, self).create(vals)
        # and len(res.kot_order_list)
        if res.name.type == 'product':
            if len(res.o_list.stock_picking_id) == 0:
                stock_picking_id = self.env['stock.picking'].create(
                    {'origin': res.kot_order_list.orderno,
                     'picking_type_id': self.env.ref('stock.picking_type_out').id,
                     'location_id': self.env.ref('stock.stock_location_stock').id,
                     'location_dest_id': self.env.ref('stock.stock_location_customers').id,
                     }
                )
                res.o_list.write({'stock_picking_id': stock_picking_id.id})
            else:
                stock_picking_id = res.o_list.stock_picking_id

            move_id = {
                'name': 'restaurant',
                'picking_id': stock_picking_id.id,
                'product_id': res.name.product_id.id,
                'product_uom_qty': res.item_qty,
                'product_uom': res.name.uom_id.id,
                'location_id': self.env.ref('stock.stock_location_stock').id,
                'location_dest_id': self.env.ref('stock.stock_location_customers').id,

            }
            move_id = self.env['stock.move'].create(move_id)
            move_id.action_confirm()
            move_id.force_assign()
            res.move_id = move_id.id
        return res

    @api.one
    def return_move(self):
        _logger.info(self.move_id)
        if self.move_id.state == 'done':

            return_picking = self.env['stock.return.picking'].with_context(
                {'active_id': self.move_id.picking_id.id,
                 'active_ids': [self.move_id.picking_id.id]}).create({

                     'product_return_moves': [(0, 0, {'product_id': self.move_id.product_id.id,
                                                      'quantity': self.move_id.product_uom_qty,
                                                      'product_uom': self.move_id.product_uom.id,
                                                      'move_id': self.move_id.id})]
                 })
            r = return_picking.with_context(
                {'active_id': self.move_id.picking_id.id,
                 'active_ids': [self.move_id.picking_id.id]}).create_returns()
            pick_id = self.env['stock.picking'].browse(r['res_id'])

            for pack in pick_id.pack_operation_ids:
                if pack.product_qty > 0:
                    pack.write({'qty_done': pack.product_qty})
                else:
                    pack.unlink()
            pick_id.do_transfer()

        else:
            self.move_id.do_unreserve()
        self.state = 'cancel'
        return True

    @api.one
    def action_preparation(self):
        self.state = 'preparation'

    @api.one
    def action_ok(self):
        self.state = 'ok'

    @api.multi
    def action_done(self):
        for order_list in self:
            if order_list.state != 'cancel':
                if len(order_list.move_id):
                    order_list.move_id.action_done()
                order_list.state = 'done'
