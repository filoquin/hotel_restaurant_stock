# -*- coding: utf-8 -*-
import werkzeug

from odoo import fields, http, _
from odoo.addons.website.models.website import slug
from odoo.http import request


class WebsiteEventController(http.Controller):


    @http.route(['/kot',
                 '/kot/<string:dummie>',
                 '/kot/<string:dummie>/<string:dummie2>',
                 '/kot/<string:dummie>/<string:dummie2>/<string:dummie3>',
                 '/kot/<string:dummie>/<string:dummie2>/<string:dummie3>/<string:dummie4>'], type='http', auth="user", website=True)
    def kot(self, dummie=None, dummie2=None, dummie3=None, dummie4=None, **kw):
        return request.render("hotel_restaurant_stock.kot")



