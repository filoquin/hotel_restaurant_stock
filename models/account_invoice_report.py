from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)


class account_invoice_report(models.Model):

    _inherit = "account.invoice.report"

    paid = fields.Float(
        string='Pagado',
    )

    def _select(self):
        select_str = super(account_invoice_report, self)._select()
        select_str += """, sub.price_total - sub.residual as paid"""
        return select_str
