from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)


class res_partner(models.Model):

    _inherit = "res.partner"

    tipo_doc = fields.Selection(
        [('DNI', 'DNI'), ('CUIT', 'CUIT'), ('Pasaporte', 'Pasaporte')],
        string='Tipo documento',
        default='DNI'
    )

    numero_doc = fields.Char(
        string='Numero documento',
    )
