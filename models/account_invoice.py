from odoo import models, fields, api, _

import logging

_logger = logging.getLogger(__name__)


class account_invoice(models.Model):

    _inherit = "account.invoice"

    @api.multi
    def _calc_paid(self):
        for record in self:
	        record.paid = record.amount_total_signed - record.residual_signed

    paid = fields.Monetary('Pagado',compute='_calc_paid')
    


